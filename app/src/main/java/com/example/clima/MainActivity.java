package com.example.clima;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


    // Declarar componentes

    private EditText alimento;
    private TextView nomreceta;
    private TextView receta;
    private Button aceptar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.alimento = findViewById(R.id.editAlimento);
        this.aceptar = findViewById(R.id.btnAc1);
        this.nomreceta = findViewById(R.id.textNomRe);
        this.receta = findViewById(R.id.textReceta);


        this.aceptar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String alimen = alimento.getText().toString().trim();

                String url = "https://api.edamam.com/search?q="+ alimen +"&app_id=da6bbbb6&app_key=869c3f1b9cdbc11ed50806b446de8152";

                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response){
                                try {
                                    JSONObject respJSON = new JSONObject(response);
                                    JSONArray hits = respJSON.getJSONArray( "hits");
                                    JSONObject inRec = hits.getJSONObject(0);
                                    JSONObject recipe = inRec.getJSONObject("recipe");
                                    JSONArray ingredientLines = recipe.getJSONArray("ingredientLines");
                                    String label = recipe.getString("label");

                                    String nomRec =label;
                                    nomreceta.setText(nomRec);
                                    String nomIngren = "";

                                    for(int i = 0; i < ingredientLines.length(); i++){

                                        nomIngren += ingredientLines.getString(i) + "\n";

                                        receta.setText(nomIngren);
                                    }
                                    //Log.d("TAG_INGREDIENTE", "ingrediente n °" + nomIngren + "\n");



                                }catch (JSONException e){
                                    e.printStackTrace();
                                }

                            }

                        },


                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //reporte error

                            }
                        }
                );
                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);
            }
        });
    }
}
